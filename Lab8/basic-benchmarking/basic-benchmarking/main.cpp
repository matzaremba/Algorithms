// Architectures and Performance: Basic Benchmarking lab exercise
// Adam Sampson <a.sampson@abertay.ac.uk>

#include <chrono>
#include <iostream>
#include <fstream>
#include <thread>
#include <list>
#include <ctime>

// Import things we need from the standard library
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::cout;
using std::endl;
using std::this_thread::sleep_for;

// Define the alias "the_clock" for the clock type we're going to use.
// (You can change this to make the code below use a different clock.)
typedef std::chrono::steady_clock the_clock;
typedef std::list<int> List;

void InsertOrdered(List& list, int arg);
List InsertionSort(List list);

int list_size = 100;
int iteration_size = 1000;

int main(int argc, char *argv[])
{
	//std::ofstream outputFile("out5.csv");
	std::ofstream outputFile;
	outputFile.open("out5.csv", std::ofstream::out | std::ofstream::app);

	srand(static_cast<unsigned int>(time(NULL)));

	for (int N = 1; N < iteration_size; N *= 2)
	{
		// List to populate with random numbers
		List input;
		// Populating with random numbers
		for (int i = 0; i < N; ++i) {
			input.push_front((rand() % N) + 1);
		}

		// Start timing
		the_clock::time_point start = the_clock::now();

		// Do something that takes some time
		List output = InsertionSort(input);
		// Stop timing
		the_clock::time_point end = the_clock::now();

		// Compute the difference between the two times in milliseconds
		auto time_taken = duration_cast<milliseconds>(end - start).count();
		cout << "It took " << time_taken << " ms to sort " << N << " items" << endl;
		outputFile << N << "," << time_taken << endl;
		input.empty(); list_size *= 2;
	}
	outputFile.close();


	return 0;
}

void InsertOrdered(List& list, int item) {
	auto it = list.begin();
	while (it != list.end()) {
		if (*it >= item) {
			// Found the item
			list.insert(it, item);
			return;
		}
		it++;
	}
	// Not found - add to the end
	list.push_back(item);
}

List InsertionSort(List list) {
	List output;
	for (int i : list) {
		InsertOrdered(output, i);
	}
	return output;
}

