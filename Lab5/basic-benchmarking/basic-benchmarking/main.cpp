// Architectures and Performance: Basic Benchmarking lab exercise
// Adam Sampson <a.sampson@abertay.ac.uk>

#include <chrono>
#include <iostream>
#include <thread>
#include <stack>
#include <vector>
#include <array>
//#define NDEBUG	  // disable all assert calls
//#include <assert.h> // C version
#include <cassert>	  // C++ version
// Import things we need from the standard library
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::cout;
using std::endl;
using std::this_thread::sleep_for;

// Define the alias "the_clock" for the clock type we're going to use.
// (You can change this to make the code below use a different clock.)
typedef std::chrono::steady_clock the_clock;

void testStack();

template <typename T> class CustomStack
{
private:
	//T stack[8];
	std::array<T, 8> stack;
	//std::vector<T> stack;
	int counter = 0;
public:
	bool empty() {
		if (counter == 0) { return true; }
		else { return false; }
	}

	void push(T value) {
		stack[counter] = value;
		counter++;

		return;
	}

	void pop() {
		counter--;

		return;
	}

	T& top() { // return by reference
		return stack[counter - 1];
	}

	T size() {
		return sizeof(stack) / sizeof(stack[0]);
	}

};

int main(int argc, char *argv[])
{
	// Start timing
	the_clock::time_point start = the_clock::now();

	// Do something that takes some time
	testStack();

	// Stop timing
	the_clock::time_point end = the_clock::now();

	// Compute the difference between the two times in milliseconds
	auto time_taken = duration_cast<milliseconds>(end - start).count();
	cout << "It took " << time_taken << " ms." << endl;

	return 0;
}

void testStack() {
	CustomStack<float> myStack;
	assert(abs(42) == 42);
	assert(abs(-42) == 42);

	assert(myStack.empty());
	myStack.push(42);
	//assert(myStack.empty()); //pops an error because stack is NOT empty. Assert macro pops an error is the expresion returns false (i.e. equals zero)
	assert(!myStack.empty());  //doesnt pop an error

	std::cout << "myStack.top() is now " << myStack.top() << endl; 

	std::cin.get();
	return;
}

// written by Adam Sampson
/*float a[10];
float *f = &a[0];
f++; // moves pointer to the next item in array
float *f = a;*/

//int* pTop = &(myStack.top());
//if (*pTop == myStack.top())
//int* pTop = &(myStack.top());
//if (pTop != nullptr)


