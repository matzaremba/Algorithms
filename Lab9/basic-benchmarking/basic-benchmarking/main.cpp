// Architectures and Performance: Basic Benchmarking lab exercise
// Adam Sampson <a.sampson@abertay.ac.uk>

#include <chrono>
#include <iostream>
#include <fstream>
#include <thread>
#include <list>
#include <ctime>
#include <array>
#include <curses.h>

// Import things we need from the standard library
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::cout;
using std::endl;
using std::this_thread::sleep_for;


struct Vertex {
    std::string name;
    bool blocked;
    int distance; // -1 if not filled in yet
    std::list<Vertex *> neighbours;

    Vertex(std::string name_)
    : name(name_), blocked(false), distance(-1) {}
};

void connect(Vertex& a, Vertex& b) {
    a.neighbours.push_back(&b);
    b.neighbours.push_back(&a);
}

void display(std::list<Vertex *>& graph) {
    for (auto v : graph) {
        std::cout << v->name << ": " << v->distance << std::endl;
    }
}

void test() {
    Vertex va("A"), vb("B"), vc("C"), vd("D");
    connect(va, vb);
    connect(vb, vc);
    connect(va, vd);
    connect(vd, vc);
    vd.blocked = true;
    std::list<Vertex *> graph { &va, &vb, &vc, &vd };
    display(graph);
}

struct Coord {
	int x, y;
};

std::list<Coord> path;

const int width = 10, height = 10;
int grid[width][height];

void displayGrid() {
	for (int i = 0; i < width; ++i) {
		for (int j = 0; j < height; ++j) {
			cout << grid[i][j] << "  ";
		}
		cout << endl << endl;
	}
}

void initialiseGrid() {
	for (int i = 0; i < width; ++i) {
		for (int j = 0; j < height; ++j) {
			grid[i][j] = -1;
			if (i % 2 == 0 && j % 2 == 0 && i != 0 && j != 0) {
				grid[i][j] = (-2);
			}
		}
	}
}

void checkCell(int x, int y, int value) {
	if (x < 0 || y < 0 || x >= width || y >= height) {
		return; // Not a valid position.
	}

	if (grid[x][y] == -1) {
		grid[x][y] = value;
	}
}

void checkCell2(int x, int y) {
	if (x < 0 || y < 0 || x >= width || y >= height) {
		return; // Not a valid position.
	}

	Coord temp;

	if (grid[x-1][y-1] < grid[x][y] && grid[x - 1][y - 1] != (-2) && grid[x][y] != (-2)) {
		temp.x = x; temp.y = y;
		path.push_back(temp);
	}
}
void secondPhase(Coord start, Coord end) {
	int i = end.x;
	int j = end.y;

	while (i >= 0 && j >= 0) {
		displayGrid();
		for (auto it = path.begin(); it != path.end(); ++it) {
			Coord temp = *it;
			cout << temp.x << " " << temp.y << endl;
		}
		std::cin.get();

		checkCell2(i - 1, j - 1);
		checkCell2(i - 1, j);
		checkCell2(i - 1, j + 1);
		checkCell2(i, j - 1);
		checkCell2(i, j + 1);
		checkCell2(i + 1, j - 1);
		checkCell2(i + 1, j);
		checkCell2(i + 1, j + 1);
		i--; j--;
	}
	cout << start.x << " " << start.y << endl;
}

void firstPhase(Coord start, Coord end) {
	grid[start.x][start.y] = 0; // write 0 into the starting cell
	if (grid[end.x][end.y] != -1) { grid[end.x][end.y] = -1; };

	for (int loop = 0;; ++loop) {
		system("cls");
		displayGrid();
		//std::cin.get();

		for (int i = 0; i < width; ++i) {
			for (int j = 0; j < height; ++j) {
				if (grid[i][j] == loop) {
					checkCell(i - 1, j - 1, loop + 1);
					checkCell(i - 1, j, loop + 1);
					checkCell(i - 1, j + 1, loop + 1);
					checkCell(i, j - 1, loop + 1);
					checkCell(i, j + 1, loop + 1);
					checkCell(i + 1, j - 1, loop + 1);
					checkCell(i + 1, j, loop + 1);
					checkCell(i + 1, j + 1, loop + 1);
					if (i == end.x && j == end.y)
						return;
				}
			}
		}
	}
}

int main(int argc, char *argv[]) {
	srand(static_cast<unsigned int>(time(NULL)));

//	Coord start{ 0, 0 };
//	Coord end{ 4, 4 };
//	initialiseGrid();
//	firstPhase(start, end);
// secondPhase(start, end);


    test();
    std::cin.get();

	return 0;
}
