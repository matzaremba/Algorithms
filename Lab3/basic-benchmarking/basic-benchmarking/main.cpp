// Architectures and Performance: Basic Benchmarking lab exercise
// Adam Sampson <a.sampson@abertay.ac.uk>

#include <chrono>
#include <iostream>
#include <thread>
#include <vector>
#include <list>
#include <algorithm>

// Import things we need from the standard library
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::cout;
using std::endl;
using std::this_thread::sleep_for;

// Define the alias "the_clock" for the clock type we're going to use.
// (You can change this to make the code below use a different clock.)
typedef std::chrono::steady_clock the_clock;


// The size of the array
const int HEIGHT = 4000; // height/Y comes first
const int WIDTH = 2000;  // width/X comes second

float array[HEIGHT][WIDTH];
std::list<float> results;
//std::vector<float> results;


int main(int argc, char *argv[])
{
	float sum = 0;
	// Fill the array with data
	for (int y = 0; y < HEIGHT; ++y) {
		for (int x = 0; x < WIDTH; ++x) {
			array[y][x] = float(x + y);
		}
	}

			for (int i = 0; i < 10; ++i) {
			// Start timing
			the_clock::time_point start = the_clock::now();

			// Do something that takes some time
			//sleep_for(milliseconds(303));
			for (int y = 0; y < HEIGHT; ++y) { // HEIGHT = 2000
				for (int x = 0; x < WIDTH; ++x) { // WIDTH = 1000
					sum += array[y][x];
					}
			}

			// Stop timing
			the_clock::time_point end = the_clock::now();

			// Compute the difference between the two times in milliseconds
			
			auto time_taken = duration_cast<milliseconds>(end - start).count();
			results.push_front(time_taken);
			
			//cout << "It took " << time_taken << " ms." << endl;
		}
		//std::sort(results.begin(), results.end());
		results.sort();
		for (auto i : results) cout << "It took " << i << " ms." << endl;
		//cout << "sum: " << sum << endl;

	std::cin.get();

	return 0;
}