// Architectures and Performance: Basic Benchmarking lab exercise
// Adam Sampson <a.sampson@abertay.ac.uk>

#include <chrono>
#include <iostream>
#include <thread>
#include <vector>
#include <list>
#include <deque>
#include <set>

// Import things we need from the standard library
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::cout;
using std::endl;
using std::this_thread::sleep_for;

// Define the alias "the_clock" for the clock type we're going to use.
// (You can change this to make the code below use a different clock.)
typedef std::chrono::steady_clock the_clock;
void testStuff();
void testAuto();

int main(int argc, char *argv[])
{
	// Start timing
	the_clock::time_point start = the_clock::now();

	// Do something that takes some time
	testStuff();
	// Stop timing
	the_clock::time_point end = the_clock::now();

	// Compute the difference between the two times in milliseconds
	auto time_taken = duration_cast<milliseconds>(end - start).count();
	cout << "It took " << time_taken << " ms." << endl;

	return 0;
}

void testAuto() {
	//float radius = 1.0f;
	//vs.
	auto radius = 1.0f;

	std::set<float> bananas { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	//std::vector<float>::const_reverse_iterator it = bananas.crbegin();
	//vs.
	auto it = bananas.crbegin();

	for (auto i : bananas) {
		std::cout << i << endl;
	}

	for (auto i : boost::adaptors::reverse)

	return;
}


void testStuff() {
	std::list<int> things { 42, 17, 33, 45, 78, 3, 19, 0 };
	std::list<int>::reverse_iterator it = things.rbegin();

	int sum = 0;

	for (int i = 0; i < things.size(); ++i) {
		//sum += things.at(i);
	}

	while (it != things.rend()) {
		std::cout << *it << endl;
		it++;
	}

	for (it = things.rbegin(); it != things.rend(); it++)
		std::cout << *it << endl;

	for (auto i : things) {
		std::cout << i << endl;
	}
	//std::cout << things.at(2);
	//std::cout << sum;
	return;
}
	

//sleep_for(milliseconds(300));